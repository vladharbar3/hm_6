from django.db import connections
from django.views import generic


def get_genres():
    sql_txt = 'SELECT SUM(UnitPrice) as price, SUM(Quantity) as count, SUM(UnitPrice) * SUM(Quantity) as sum' \
              ' FROM invoice_items'

    with connections['default'].cursor() as cursor:
        cursor.execute(sql_txt)
        row = cursor.fetchall()

    return {'Price': row[0][0], 'count': row[0][1], 'sum': row[0][2]}


class IndexView(generic.ListView):
    template_name = 'task2/index.html'
    context_object_name = 'result'

    def get_queryset(self):
        """Return the last five published questions."""
        return get_genres()
