from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('task2/', include('task2.urls')),
    path('task1/', include('task1.urls')),
    path('task3/', include('task3.urls')),
    path('admin/', admin.site.urls),


]