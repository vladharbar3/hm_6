from django.db import connections
from django.views import generic


def get_greatest_hits(limit=10):
    sql_txt = 'SELECT tracks.Name, invoice_items.Quantity ' \
              'FROM invoice_items ' \
              'LEFT JOIN tracks ON tracks.TrackId = invoice_items.TrackId ' \
              'GROUP BY tracks.Name LIMIT 10'

    with connections['default'].cursor() as cursor:
        cursor.execute(sql_txt)
        row = cursor.fetchall()

    result = []
    for rec in row:
        result.append({'track': rec[0], 'Quantity': rec[1]})

    return result


class IndexView(generic.ListView):
    template_name = 'task3/index.html'
    context_object_name = 'result'

    def get_queryset(self):
        """Return the last five published questions."""
        return get_greatest_hits()
