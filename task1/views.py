from django.db import connections
from django.views import generic


def get_sales():
    sql_txt = 'SELECT genres.Name, SUM(tracks.Milliseconds) ' \
              'FROM tracks ' \
              'LEFT JOIN genres ON tracks.GenreId = genres.GenreId ' \
              'GROUP BY genres.Name'

    with connections['default'].cursor() as cursor:
        cursor.execute(sql_txt)
        row = cursor.fetchall()

    result = []
    for rec in row:
        result.append({'name': rec[0], 'track': rec[1]})

    return result


class IndexView(generic.ListView):
    template_name = 'task1/index.html'
    context_object_name = 'result'

    def get_queryset(self):
        """Return the last five published questions."""
        return get_sales()
